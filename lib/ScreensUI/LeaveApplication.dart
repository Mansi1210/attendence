import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LeaveApplication extends StatefulWidget {
  const LeaveApplication({Key key}) : super(key: key);

  @override
  _LeaveApplicationState createState() => _LeaveApplicationState();
}

class _LeaveApplicationState extends State<LeaveApplication> {
  bool valueFirst = false;
  bool valueSecond =false;
  bool valueThird =false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          "Leave Application",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.purple,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Center(
                child: Text(
              "Available Leaves",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            )),
          ),
          SizedBox(
            height: 8,
          ),
          leavesButtons(),
          managerDetail(),
          fillDate(),
          selectDate(),
          leaveDetails(),
          submitButton()
        ],
      ),
    );
  }

  Widget leavesButtons() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: Container(
            height: 40,
            width: 60,
            child: Center(
                child: Text(
              "CL-1",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            )),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: Colors.purpleAccent,
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(3.0, 3.0),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey,
                  ),
                ]),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: Container(
            height: 40,
            width: 60,
            child: Center(
                child: Text(
              "AL-2",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            )),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: Colors.purpleAccent,
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(3.0, 3.0),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey,
                  ),
                ]),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: Container(
            height: 40,
            width: 60,
            child: Center(
                child: Text(
              "ML-0",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            )),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: Colors.purpleAccent,
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(3.0, 3.0),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey,
                  ),
                ]),
          ),
        ),
      ],
    );
  }

  Widget managerDetail() {
    return Column(
      children: [
        Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 15, 0, 0),
              child: Text(
                "Your Manager",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Colors.black),
                textAlign: TextAlign.left,
              ),
            )),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
          child: Container(
            height: 45,
            width: double.infinity,
            decoration: BoxDecoration(border: Border.all()),
            child: TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: "write your manager name"),
            ),
          ),
        ),
      ],
    );
  }


  Widget fillDate() {
    return Column(
      children: [
        Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 15, 0, 0),
              child: Text(
                "Today's  Date",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Colors.black),
                textAlign: TextAlign.left,
              ),
            )),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5),
          child: Container(
            height: 45,
            width: double.infinity,
            decoration: BoxDecoration(border: Border.all()),
            child: TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(hintText: " fill today's date"),
            ),
          ),
        ),
      ],
    );
  }

  Widget selectDate(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:35.0,vertical: 15),
      child: Row(
        children: [
          Text("From",style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 13,
          color: Colors.black),
      textAlign: TextAlign.left,),
          Padding(
            padding: const EdgeInsets.only(left:18.0),
            child: Container(
              height: 35,
              width: 80,
              child: Center(
                  child: Text(
                    "Select",
                    style:
                    TextStyle(color: Colors.purpleAccent, fontWeight: FontWeight.bold),
                  )),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: const Offset(3.0, 3.0),
                      blurRadius: 4.0,
                      spreadRadius: 2.0,
                      color: Colors.grey,
                    ),
                  ]),
            ),
          ),
           Padding(
             padding: const EdgeInsets.symmetric(horizontal: 18.0),
             child: Text("To",style: TextStyle(
                 fontWeight: FontWeight.bold,
                 fontSize: 13,
                 color: Colors.black),
               textAlign: TextAlign.left,),
           ),
          Container(
            height: 35,
            width: 80,
            child: Center(
                child: Text(
                  "Select",
                  style:
                  TextStyle(color: Colors.purpleAccent, fontWeight: FontWeight.bold),
                )),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(3.0, 3.0),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey,
                  ),
                ]),
          ),
        ],
      ),
    );
  }

  Widget leaveDetails(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0,vertical: 15),
      child: Column(
        children: [
          Container(
              alignment: Alignment.centerLeft,
              child: Padding(
            padding: const EdgeInsets.only(left:8.0),
            child: Text("Number of days on leave :-",style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
                color: Colors.black),
              textAlign: TextAlign.left,),
          )),
          Container(
              alignment: Alignment.centerLeft,
              child: Padding(
            padding: const EdgeInsets.fromLTRB(8,15,0,15),
            child: Text("Type of leave",style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
                color: Colors.black),
              textAlign: TextAlign.left,),
          )),
          Row(
            children: [
              Checkbox(
                checkColor: Colors.white,
                activeColor: Colors.purple,
                value: this.valueFirst,
                onChanged: (bool value) {
                  setState(() {
                    this.valueFirst = value;
                  });
                },
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0),
                child: Text("Medical leave",style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Colors.black),
                  textAlign: TextAlign.left,),
              )
            ],
          ),
          Row(
            children: [
              Checkbox(
                checkColor: Colors.white,
                 activeColor: Colors.purple,
                value: this.valueSecond,
                onChanged: (bool value) {
                  setState(() {
                    this.valueSecond = value;
                  });
                },
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0),
                child: Text("Annual leave",style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Colors.black),
                  textAlign: TextAlign.left,),
              )
            ],
          ),
          Row(
            children: [
              Checkbox(
                checkColor: Colors.white,
                 activeColor: Colors.purple,
                value: this.valueThird,
                onChanged: (bool value) {
                  setState(() {
                    this.valueThird = value;
                  });
                },
              ),
              Padding(
                padding: const EdgeInsets.only(left:18.0),
                child: Text("Casual leave",style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Colors.black),
                  textAlign: TextAlign.left,),
              )
            ],
          ),
          TextField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(hintText: "Message for Management(optional)",fillColor: Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget submitButton(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 38.0),
      child: Container(
        height: 40,
        width: double.infinity,
        child: Center(
            child: Text(
              "Submit",
              style:
              TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            )),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            color: Colors.purpleAccent,
            boxShadow: [
              BoxShadow(
                offset: const Offset(3.0, 3.0),
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: Colors.grey,
              ),
            ]),
      ),
    );
  }
}
