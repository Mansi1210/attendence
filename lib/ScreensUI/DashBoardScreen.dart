import 'package:attendence_app/ScreensUI/LeaveApplication.dart';
import 'package:attendence_app/ScreensUI/LoginScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  SharedPreferences prefs;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  //  initial();
  }

  // void initial() async {
  //   var loginData = await SharedPreferences.getInstance();
  //   setState(() {
  //     username = logindata.getString('username');
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.purple,
        appBar: AppBar(
          title: Text(
            "DASHBOARD",
            style: TextStyle(
                color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Colors.purple,
        ),
        body: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 30.0, vertical: 30),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [attendenceRecoder(), attendenceSummary()],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LeaveApplication()),
                      );
                    },
                    child: leaveApplication(),
                  ),
                  leaveStatus(),
                ],
              ),
            ),

          ],
        ),
        drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.purple,
          ),
          child: Drawer(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 100,
                ),
                dashboardLocation(),
                SizedBox(
                  height: 20,
                ),
                review(),
                SizedBox(
                  height: 20,
                ),
                profileEdit(),
                SizedBox(
                  height: 20,
                ),
                logOut(context)
              ],
            ),
          ),
        ));
  }

  Widget attendenceRecoder() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        height: 150,
        width: 140,
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              children: [
                Container(height: 50, child: Image.asset("recoder.png")),
                Text(
                  "Attendence\n Recorder",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Make your in\nand out",
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget attendenceSummary() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        height: 150,
        width: 140,
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              children: [
                Container(height: 50, child: Image.asset("summary.png")),
                Center(
                    child: Text(
                  "Attendence\n Summary",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )),
                Center(
                    child: Text(
                  "Check your previous\n record",
                  textAlign: TextAlign.center,
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget leaveApplication() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        height: 150,
        width: 140,
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              children: [
                Container(height: 50, child: Image.asset("leave.jpg")),
                Text(
                  "Leaves\n Application",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Text("Management")
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget leaveStatus() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        height: 150,
        width: 140,
        child: Card(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              children: [
                Container(height: 50, child: Image.asset("leaveStatus.png")),
                Text(
                  "Leave Status",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Check pending\n Status of leaves",
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget dashboardLocation() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.purpleAccent)),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(Icons.location_on_rounded, color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "location",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget review() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.white)),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(Icons.person_outline, color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "Review Pending Leaves",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget profileEdit() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.white)),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(Icons.person_outline_rounded, color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "Edit your Profile",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget logOut(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.white)),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(
                Icons.logout,
                color: Colors.white,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: InkWell(
                onTap: () async{
                  SharedPreferences prefs =await SharedPreferences.getInstance();
                  prefs.setBool("login", true);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
                child: Text(
                  "Logout",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
