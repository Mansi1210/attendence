

class User {
  String emailText;
  String psswrd;

  User({this.emailText, this.psswrd});

  Map<String, dynamic> toJson() {
    return {"emailText": emailText, "psswrd": psswrd};
  }

  User.fromJson(Map<String, dynamic> json) {
    emailText = json["emailText"];
    psswrd = json["psswrd"];
  }
}
