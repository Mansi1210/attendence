import 'dart:convert';

import 'package:attendence_app/ScreensUI/DashBoardScreen.dart';
import 'package:attendence_app/ScreensUI/json.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
SharedPreferences prefs;

bool newuser;
  var emailId = TextEditingController();
  var psswrd = TextEditingController();

  FocusNode email = FocusNode();
  FocusNode pass = FocusNode();

  bool obscureText = true;
  IconData eye = Icons.visibility_off;

  onClickLoginButton() async {
    String emailText = emailId.text;
    String password = psswrd.text;

    email.requestFocus();
    pass.requestFocus();

    if (emailText.isEmpty) {
      print("email can't be empty");
      email.requestFocus();
    } else if (!validateEmail(emailText)) {
      print("email is invalid");
      emailId.clear();
    } else if (password.isEmpty) {
      print("pass can't be empty");
    } else if (password.length < 6) {
      print("pass is invalid");
      pass.requestFocus();
      psswrd.clear();
    }
    else  {
      print('Successfull');
      SharedPreferences prefs  = await SharedPreferences.getInstance();
      prefs.setBool('login', false );
      prefs.setString('username', emailText);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => Dashboard()));
    }

  }

  @override
  void initState() {
    super.initState();
    check_if_already_login();
  }
  void check_if_already_login() async {
     SharedPreferences prefs = await SharedPreferences.getInstance();
    newuser = (prefs.getBool('login') ?? true);
    print(newuser);
    if (newuser == false) {
      Navigator.pushReplacement(
          context, new MaterialPageRoute(builder: (context) => Dashboard()));
    }
  }

  //
  // loadData()async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String json = prefs.get("keys");
  //   print("loaded data $json");
  //
  //   if (json == null) {
  //     print("no data in prefrece");
  //   } else {
  //     Map<String, dynamic> map = jsonDecode(json);
  //     print("map $map");
  //   }
  // }
 saveData() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final testUser = User(
    emailText: emailId.text,
    psswrd: psswrd.text
  );
  String json = jsonEncode(testUser);
  print(json);
  prefs.setString("keys", json);
 }

  @override
  void dispose() {
    emailId.dispose();
    psswrd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.purple,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: () {
          closeKeyboard();
        },
        child: Column(
          children: [
            SizedBox(height: screenSize.height / 10),
            topBar(),
            loginBox(context),
            SizedBox(
              height: screenSize.height / 20,
            ),
            loginButton(),
            SizedBox(
              height: screenSize.height / 30,
            ),
            retriveButton(),
            SizedBox( height: screenSize.height / 30,),
            optionText()
          ],
        ),
      ),
    );
  }

  Widget topBar() {
    return Container(
      width: double.infinity,
      height: 90,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 80.0),
        child: Text(
          "ATTENDENCE",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 35),
        ),
      ),
    );
  }

  Widget loginBox(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        height: screen.height / 3,
        width: double.infinity,
        child: Card(
          child: Column(
            children: [
              Text(
                "Login",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(
                height: screen.height / 80,
              ),
              emailFill(),
              SizedBox(
                height: screen.height / 50,
              ),
              passFill(),
              SizedBox(
                height: screen.height / 20,
              ),
              forgetPss()
            ],
          ),
        ),
      ),
    );
  }

  Widget emailFill() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
          child: Icon(
            Icons.person,
            color: Colors.purple,
          ),
        ),
        Expanded(
          flex: 2,
          child: TextField(
            controller: emailId,
            // focusNode: email,
            decoration: InputDecoration(
              hintText: "Enter your email id",
              fillColor: Colors.grey,
            ),
            keyboardType: TextInputType.emailAddress,
          ),
        )
      ],
    );
  }

  Widget passFill() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
          child: Icon(
            Icons.lock,
            color: Colors.purple,
          ),
        ),
        Expanded(
          flex: 2,
          child: TextField(
            controller: psswrd,
            focusNode: pass,
            decoration: InputDecoration(
              hintText: "Enter your password",
              fillColor: Colors.grey,
            ),
          ),
        )
      ],
    );
  }

  Widget forgetPss() {
    return Container(
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            "Reset",
            style: TextStyle(color: Colors.purple),
          ),
          Text("Forgot Password?", style: TextStyle(color: Colors.purple))
        ],
      ),
    );
  }

  Widget loginButton() {
    return Container(
      height: 50,
      width: 120,
      child: Card(
        color: Colors.purpleAccent,
        child: Center(
            child: InkWell(
                onTap: () {
                  onClickLoginButton();
                },
                child: Text("LOGIN",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20)))),
      ),
    );
  }

  Widget retriveButton() {
    return Container(
      height: 50,
      width: 120,
      child: Card(
        color: Colors.purpleAccent,
        child: Center(
            child: InkWell(
                onTap: () {
                  saveData();
                },
                child: Text("data",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20)))),
      ),
    );
  }


  Widget optionText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            line(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Center(
                  child: Text(
                "Other Options",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              )),
            ),
            line()
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 40),
          child: Text(
            "Don't have Login Details? Contact Admin",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Widget line() {
    return Container(
      width: 80,
      height: 1,
      color: Colors.white,
    );
  }

  bool validateEmail(String emailId) {
    if (emailId.isEmpty) {
// The form is empty
      return false;
    }
// This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(emailId)) {
// So, the email is valid
      return true;
    } else {
      return false;
    }
  }

  Widget closeKeyboard() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }
}
