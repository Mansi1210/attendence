import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:attendence_app/ScreensUI/LoginScreen.dart';
import 'package:flutter/material.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;
  String emailId;
  bool newUser;


  void initial() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      emailId = prefs.getString('email');
    });
  }
  @override
  void initState() {
    super.initState();

    initial();
    Timer(
        Duration(seconds: 5),
        () =>
            Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen())));
  }

  @override
  Widget build(BuildContext context) {

    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          SizedBox(
            height: screenSize.height / 9,
          ),
          Center(
            child: Text("Welcome",
                style: TextStyle(
                  fontSize: 30,
                )),
          ),
          Container(
              height: 550,
              child: Center(
                  child: Image.asset(
                "icon.jpg",
                fit: BoxFit.fill,
              ))),
          // CircularProgressIndicator()
        ],
      ),
    );
  }
}
