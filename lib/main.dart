
import 'package:attendence_app/ScreensUI/SplashScreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ATTENDENCE',
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
